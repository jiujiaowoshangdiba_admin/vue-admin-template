/* 利用内置的 keep-alive 组件对指定页面进行缓存
 * @Author: RuiLin
 * @Date: 2021-01-27 16:36:39
 * @Last Modified by: RuiLin
 * @Last Modified time: 2021-01-28 14:50:10
 */
const state = {
  cachedViews: []
}
const mutations = {
  // 添加组件缓存
  ADD_CACHED_VIEW: (state, view) => {
    const name = view.name
    if (state.cachedViews.includes(name)) return
    if (view.meta.cache) {
      state.cachedViews.push(name)
    }
  },

  // 移除组件缓存
  DEL_CACHED_VIEW: (state, view) => {
    const index = state.cachedViews.indexOf(view.name)
    index > -1 && state.cachedViews.splice(index, 1)
  }
}
export default {
  namespaced: true,
  state,
  mutations
}
