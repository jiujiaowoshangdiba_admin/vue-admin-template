import request from '@/utils/request'

/**
 * 上传接口
 * @param {*} data
 */
export function upload(data) {
  return request({
    url: `/vue-admin-template/upload`,
    method: 'post',
    data
  })
}
