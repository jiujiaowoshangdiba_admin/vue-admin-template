import Mock from 'mockjs'

const data = Mock.mock({
  'items|30': [{
    id: '@id',
    title: '@sentence(10, 20)',
    'status|1': ['published', 'draft', 'deleted'],
    author: 'name',
    display_time: '@datetime',
    pageviews: '@integer(300, 5000)'
  }]
})

export default [
  {
    url: '/vue-admin-template/table/list',
    type: 'get',
    response: config => {
      console.log('接口', config)
      const items = data.items
      return {
        code: 20000,
        result: {
          totalSize: items.length,
          pageIndex: 1,
          pageSize: 10,
          data: items
        }
      }
    }
  }
]
